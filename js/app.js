var app = angular.module('teamx', ['ngRoute']);

/*
    Routes for part pages
 */
app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homectrl'
        })
        .when('/plugins', {
            templateUrl: 'pages/plugins.html',
            controller: 'pluginsctrl'
        })
        .when('/kontakt', {
            templateUrl: 'pages/kontakt.html',
            controller: 'kontaktctrl'
        })
        .otherwise({
            templateUrl: 'pages/404.html',
            controller: '404ctrl'
        });
    $locationProvider.html5Mode({
        enabled: true
    });
});